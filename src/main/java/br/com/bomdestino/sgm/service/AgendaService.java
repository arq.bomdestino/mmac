package br.com.bomdestino.sgm.service;

import br.com.bomdestino.sgm.domain.Agenda;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link Agenda}.
 */
public interface AgendaService {
    /**
     * Save a agenda.
     *
     * @param agenda the entity to save.
     * @return the persisted entity.
     */
    Agenda save(Agenda agenda);

    /**
     * Partially updates a agenda.
     *
     * @param agenda the entity to update partially.
     * @return the persisted entity.
     */
    Optional<Agenda> partialUpdate(Agenda agenda);

    /**
     * Get all the agenda.
     *
     * @return the list of entities.
     */
    List<Agenda> findAll();
    /**
     * Get all the Agenda where Solicitacao is {@code null}.
     *
     * @return the {@link List} of entities.
     */
    List<Agenda> findAllWhereSolicitacaoIsNull();

    /**
     * Get the "id" agenda.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Agenda> findOne(Long id);

    /**
     * Delete the "id" agenda.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
