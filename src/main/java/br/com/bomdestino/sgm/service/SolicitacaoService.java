package br.com.bomdestino.sgm.service;

import br.com.bomdestino.sgm.domain.Solicitacao;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link Solicitacao}.
 */
public interface SolicitacaoService {
    /**
     * Save a solicitacao.
     *
     * @param solicitacao the entity to save.
     * @return the persisted entity.
     */
    Solicitacao save(Solicitacao solicitacao);

    /**
     * Partially updates a solicitacao.
     *
     * @param solicitacao the entity to update partially.
     * @return the persisted entity.
     */
    Optional<Solicitacao> partialUpdate(Solicitacao solicitacao);

    /**
     * Get all the solicitacaos.
     *
     * @return the list of entities.
     */
    List<Solicitacao> findAll();
    /**
     * Get all the Solicitacao where Area is {@code null}.
     *
     * @return the {@link List} of entities.
     */
    List<Solicitacao> findAllWhereAreaIsNull();

    /**
     * Get the "id" solicitacao.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Solicitacao> findOne(Long id);

    /**
     * Delete the "id" solicitacao.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
