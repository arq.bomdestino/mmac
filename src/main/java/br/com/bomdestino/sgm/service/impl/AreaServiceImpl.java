package br.com.bomdestino.sgm.service.impl;

import br.com.bomdestino.sgm.domain.Area;
import br.com.bomdestino.sgm.repository.AreaRepository;
import br.com.bomdestino.sgm.service.AreaService;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Area}.
 */
@Service
@Transactional
public class AreaServiceImpl implements AreaService {

    private final Logger log = LoggerFactory.getLogger(AreaServiceImpl.class);

    private final AreaRepository areaRepository;

    public AreaServiceImpl(AreaRepository areaRepository) {
        this.areaRepository = areaRepository;
    }

    @Override
    public Area save(Area area) {
        log.debug("Request to save Area : {}", area);
        return areaRepository.save(area);
    }

    @Override
    public Optional<Area> partialUpdate(Area area) {
        log.debug("Request to partially update Area : {}", area);

        return areaRepository
            .findById(area.getId())
            .map(existingArea -> {
                if (area.getCodArea() != null) {
                    existingArea.setCodArea(area.getCodArea());
                }
                if (area.getDescricao() != null) {
                    existingArea.setDescricao(area.getDescricao());
                }

                return existingArea;
            })
            .map(areaRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Area> findAll() {
        log.debug("Request to get all Areas");
        return areaRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Area> findOne(Long id) {
        log.debug("Request to get Area : {}", id);
        return areaRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Area : {}", id);
        areaRepository.deleteById(id);
    }
}
