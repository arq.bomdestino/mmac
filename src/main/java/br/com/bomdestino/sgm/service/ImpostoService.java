package br.com.bomdestino.sgm.service;

import br.com.bomdestino.sgm.domain.Imposto;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link Imposto}.
 */
public interface ImpostoService {
    /**
     * Save a imposto.
     *
     * @param imposto the entity to save.
     * @return the persisted entity.
     */
    Imposto save(Imposto imposto);

    /**
     * Partially updates a imposto.
     *
     * @param imposto the entity to update partially.
     * @return the persisted entity.
     */
    Optional<Imposto> partialUpdate(Imposto imposto);

    /**
     * Get all the impostos.
     *
     * @return the list of entities.
     */
    List<Imposto> findAll();

    /**
     * Get the "id" imposto.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Imposto> findOne(Long id);

    /**
     * Delete the "id" imposto.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
