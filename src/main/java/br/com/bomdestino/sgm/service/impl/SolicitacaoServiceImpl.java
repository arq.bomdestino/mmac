package br.com.bomdestino.sgm.service.impl;

import br.com.bomdestino.sgm.domain.Solicitacao;
import br.com.bomdestino.sgm.repository.SolicitacaoRepository;
import br.com.bomdestino.sgm.service.SolicitacaoService;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Solicitacao}.
 */
@Service
@Transactional
public class SolicitacaoServiceImpl implements SolicitacaoService {

    private final Logger log = LoggerFactory.getLogger(SolicitacaoServiceImpl.class);

    private final SolicitacaoRepository solicitacaoRepository;

    public SolicitacaoServiceImpl(SolicitacaoRepository solicitacaoRepository) {
        this.solicitacaoRepository = solicitacaoRepository;
    }

    @Override
    public Solicitacao save(Solicitacao solicitacao) {
        log.debug("Request to save Solicitacao : {}", solicitacao);
        return solicitacaoRepository.save(solicitacao);
    }

    @Override
    public Optional<Solicitacao> partialUpdate(Solicitacao solicitacao) {
        log.debug("Request to partially update Solicitacao : {}", solicitacao);

        return solicitacaoRepository
            .findById(solicitacao.getId())
            .map(existingSolicitacao -> {
                if (solicitacao.getCodSolicitacao() != null) {
                    existingSolicitacao.setCodSolicitacao(solicitacao.getCodSolicitacao());
                }
                if (solicitacao.getCodUsuario() != null) {
                    existingSolicitacao.setCodUsuario(solicitacao.getCodUsuario());
                }
                if (solicitacao.getTipoSolicitacao() != null) {
                    existingSolicitacao.setTipoSolicitacao(solicitacao.getTipoSolicitacao());
                }

                return existingSolicitacao;
            })
            .map(solicitacaoRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Solicitacao> findAll() {
        log.debug("Request to get all Solicitacaos");
        return solicitacaoRepository.findAll();
    }

    /**
     *  Get all the solicitacaos where Area is {@code null}.
     *  @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<Solicitacao> findAllWhereAreaIsNull() {
        log.debug("Request to get all solicitacaos where Area is null");
        return StreamSupport
            .stream(solicitacaoRepository.findAll().spliterator(), false)
            .filter(solicitacao -> solicitacao.getArea() == null)
            .collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Solicitacao> findOne(Long id) {
        log.debug("Request to get Solicitacao : {}", id);
        return solicitacaoRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Solicitacao : {}", id);
        solicitacaoRepository.deleteById(id);
    }
}
