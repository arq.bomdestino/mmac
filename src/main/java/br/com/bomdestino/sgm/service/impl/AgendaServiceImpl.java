package br.com.bomdestino.sgm.service.impl;

import br.com.bomdestino.sgm.domain.Agenda;
import br.com.bomdestino.sgm.repository.AgendaRepository;
import br.com.bomdestino.sgm.service.AgendaService;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Agenda}.
 */
@Service
@Transactional
public class AgendaServiceImpl implements AgendaService {

    private final Logger log = LoggerFactory.getLogger(AgendaServiceImpl.class);

    private final AgendaRepository agendaRepository;

    public AgendaServiceImpl(AgendaRepository agendaRepository) {
        this.agendaRepository = agendaRepository;
    }

    @Override
    public Agenda save(Agenda agenda) {
        log.debug("Request to save Agenda : {}", agenda);
        return agendaRepository.save(agenda);
    }

    @Override
    public Optional<Agenda> partialUpdate(Agenda agenda) {
        log.debug("Request to partially update Agenda : {}", agenda);

        return agendaRepository
            .findById(agenda.getId())
            .map(existingAgenda -> {
                if (agenda.getData() != null) {
                    existingAgenda.setData(agenda.getData());
                }
                if (agenda.getDescricaoPrevia() != null) {
                    existingAgenda.setDescricaoPrevia(agenda.getDescricaoPrevia());
                }

                return existingAgenda;
            })
            .map(agendaRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Agenda> findAll() {
        log.debug("Request to get all Agenda");
        return agendaRepository.findAll();
    }

    /**
     *  Get all the agenda where Solicitacao is {@code null}.
     *  @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<Agenda> findAllWhereSolicitacaoIsNull() {
        log.debug("Request to get all agenda where Solicitacao is null");
        return StreamSupport
            .stream(agendaRepository.findAll().spliterator(), false)
            .filter(agenda -> agenda.getSolicitacao() == null)
            .collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Agenda> findOne(Long id) {
        log.debug("Request to get Agenda : {}", id);
        return agendaRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Agenda : {}", id);
        agendaRepository.deleteById(id);
    }
}
