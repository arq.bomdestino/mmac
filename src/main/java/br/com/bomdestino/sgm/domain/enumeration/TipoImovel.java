package br.com.bomdestino.sgm.domain.enumeration;

/**
 * The TipoImovel enumeration.
 */
public enum TipoImovel {
    CASA,
    PREDIO,
    PROPRIEDADE_RURAL,
    LOTE,
}
