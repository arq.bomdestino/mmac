package br.com.bomdestino.sgm.domain.enumeration;

/**
 * The TipoImposto enumeration.
 */
public enum TipoImposto {
    IPTU,
    ITBI,
}
