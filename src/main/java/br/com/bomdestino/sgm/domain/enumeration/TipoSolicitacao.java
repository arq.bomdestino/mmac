package br.com.bomdestino.sgm.domain.enumeration;

/**
 * The TipoSolicitacao enumeration.
 */
public enum TipoSolicitacao {
    ALVARA,
    ATUALIZACAO_CADASTRAL,
    VALIDACAO_DOCUMENTO,
}
