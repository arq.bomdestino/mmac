package br.com.bomdestino.sgm.repository;

import br.com.bomdestino.sgm.domain.Imposto;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Imposto entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ImpostoRepository extends JpaRepository<Imposto, Long> {}
